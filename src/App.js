import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './login';
import 'antd/dist/antd.less';
function App() {
	return (
		<div
			style={{
				width: '100%',
				height: '100vh',
				display: 'flex',
				justifyContent: 'center',
				alignContent: 'center',
				alignItems: 'center',
			}}
		>
			<Login />
		</div>
	);
}

export default App;
