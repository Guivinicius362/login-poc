import React, { Component } from 'react';
import { Form, Icon, Input, Button, Card, Typography, Row, Col } from 'antd';

class LoginScreen extends Component {
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};
	render() {
		const { Title } = Typography;
		const urlImage =
			'https://s3.amazonaws.com/front.images/assertiva/assertiva-logos_logo-cor-full.png';
		const { getFieldDecorator } = this.props.form;
		return (
			<Card
				style={{
					width: '400px',
					borderRadius: '25px',
					boxShadow:
						'0 3px 3px 0 rgba(0,0,0,0.14), 0 1px 7px 0 rgba(0,0,0,0.12), 0 3px 1px -1px rgba(0,0,0,0.2)',
				}}
			>
				<Row>
					<Col span={12} offset={6}>
						<img
							className="container-login_img"
							src={urlImage}
							style={{ marginTop: '25px', marginBottom: '25px' }}
						/>
					</Col>
				</Row>
				<Row>
					<Col span={32}>
						<Form onSubmit={this.handleSubmit} className="login-form">
							<Form.Item>
								{getFieldDecorator('username', {
									rules: [
										{ required: true, message: 'Please input your username!' },
									],
								})(
									<Input
										prefix={
											<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
										}
										placeholder="Login Empresa"
										size="large"
									/>,
								)}
							</Form.Item>
							<Form.Item>
								{getFieldDecorator('username', {
									rules: [
										{ required: true, message: 'Please input your username!' },
									],
								})(
									<Input
										prefix={
											<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
										}
										placeholder="Usuario"
										size="large"
									/>,
								)}
							</Form.Item>
							<Form.Item>
								{getFieldDecorator('password', {
									rules: [
										{ required: true, message: 'Please input your Password!' },
									],
								})(
									<Input
										prefix={
											<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
										}
										type="password"
										placeholder="Password"
										size="large"
									/>,
								)}
							</Form.Item>

							<Form.Item>
								<Row type="flex" justify="space-between">
									<Col>
										<a>Esqueci minha senha</a>
									</Col>
									<Col>
										<Button type="secundary" size={18}>
											Limpar Cookies
										</Button>
									</Col>
								</Row>
							</Form.Item>
							<Form.Item>
								<Row type="flex" justify="center">
									<Col>
										<Button type="primary" htmlType="submit" size="large">
											Fazer Login
										</Button>
									</Col>
								</Row>
							</Form.Item>
						</Form>
					</Col>
				</Row>
			</Card>
		);
	}
}
const Login = Form.create({ name: 'normal_login' })(LoginScreen);
export default Login;
